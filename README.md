# gb-nest-helpers


## NestJS Swagger Decorator

This package includes a `@nestjs/swagger` decorator to simplify the documentation of query parameters. The `@ApiStandardQuery()` decorator acts as an alias to `@ApiQuery()` and wraps all the standard query parameters used by Geekbears on `find` and `count` operations.

### Usage

Import the decorator and use it on the corresponding method on the controller class.

```typescript
import { ApiStandardQuery } from '@geekbears/gb-mongoose-query-parser';

@Get()
@ApiStandardQuery()
async find(@Query() query: any): Promise<any> {
    // Do something
}

```

## License
MIT
