import { applyDecorators } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';

/**
 * Add geekbears standard query parameters
 * @description Swagger decorator. Adds all standard query parameters
 * @returns A method decorator wrapping calls to `@ApiQuery()` decorator
 */
export const ApiStandardQuery = (): MethodDecorator => {
    return applyDecorators(
        ApiQuery({
            name: 'filter',
            description: 'A stringified filter JSON object',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'populate',
            description: 'A comma-separated list of virtual fields to populate',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'deepPopulate',
            description: 'A stringified populate JSON object',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'sort',
            description: 'The sorting configuration to apply to returned elements',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'limit',
            description: 'The max amount of records to return',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'select',
            description: 'A comma-separated list of fields to select',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'skip',
            description: 'The amount of records to skip before returning',
            schema: { type: 'string' },
            required: false,
        }),
    );
};
