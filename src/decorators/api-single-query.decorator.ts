import { applyDecorators } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';

export const ApiSingleQuery = (): MethodDecorator => {
    return applyDecorators(
        ApiQuery({
            name: 'populate',
            description: 'A comma-separated list of virtual fields to populate',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'deepPopulate',
            description: 'A stringified populate JSON object',
            schema: { type: 'string' },
            required: false,
        }),
        ApiQuery({
            name: 'select',
            description: 'A comma-separated list of fields to select',
            schema: { type: 'string' },
            required: false,
        }),
    );
};
